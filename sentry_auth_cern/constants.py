from __future__ import absolute_import, print_function

from django.conf import settings

OAUTH_SERVER = 'https://oauth.web.cern.ch/OAuth'
OAUTH_API = 'https://oauthresource.web.cern.ch/api'
ACCESS_TOKEN_URL = '{0}/Token'.format(OAUTH_SERVER)
AUTHORIZE_URL = '{0}/Authorize'.format(OAUTH_SERVER)
SCOPE = 'read:user'

CLIENT_ID = getattr(settings, 'CERN_CLIENT_ID', None)
CLIENT_SECRET = getattr(settings, 'CERN_CLIENT_SECRET', None)
MEMBER_GROUPS = getattr(settings, 'CERN_MEMBER_GROUPS', list())
ADMIN_GROUPS = getattr(settings, 'CERN_ADMIN_GROUPS', list())
