from __future__ import absolute_import, print_function

from sentry.auth.view import AuthView

from .client import CERNClient


class FetchUser(AuthView):

    def __init__(self, client_id, client_secret, *args, **kwargs):
        self.client = CERNClient(client_id, client_secret)
        super(FetchUser, self).__init__(*args, **kwargs)

    def handle(self, request, helper):
        access_token = helper.fetch_state('data')['access_token']

        user = self.client.get_user(access_token)

        if not self.client.is_member(access_token):
            return helper.error('Permission denied')

        helper.bind_state('user', user)

        return helper.next_step()
