from __future__ import absolute_import

from sentry.auth import register

from .provider import CERNOAuth2Provider

register('CERN', CERNOAuth2Provider)
