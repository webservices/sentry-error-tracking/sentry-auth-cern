#!/usr/bin/env python
from setuptools import setup, find_packages

setup(
    name='sentry-auth-cern',
    version='0.1',
    description='CERN authentication provider for Sentry',
    packages=find_packages(),
    install_requires=['sentry', ],
    include_package_data=True,
    entry_points={
        'sentry.apps': [
            'auth_cern = sentry_auth_cern',
        ],
    },
)
